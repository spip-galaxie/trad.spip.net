<?php

# toutes les 5mn, verifier si des modules sont bon_a_pousser
$command = "spip salvatore:ouvrager --traductions=salvatore/traductions/traductions.txt --module=bon_a_pousser --time 2>&1 >> salvatore/log/bon_a_pousser.log";
passthru($command);

# puis toutes les 5mn verifier si il faut tirer des depots modifies
$command = "spip salvatore:recharger --traductions=salvatore/traductions/traductions.txt --changedir=/home/sites/spip.net/contrib/public_html/debardeur/tmp/updated-files/ --from=-2hour --time 2>&1 >> salvatore/log/recharger.log";
passthru($command);

