<?php


function statut_to_variant($statut) {
	switch (strtolower($statut)) {
		case 'ok':
			return 'primary';
			break;
		case 'modif':
			return 'warning';
			break;
		case 'relire':
			return 'info';
			break;
		case 'new':
			return 'danger';
			break;
		default:
			return 'secondary';
			break;
	}
}

function tradlang_affiche_progressbar($total_ok, $total_relire, $total_modif, $total_new, $etiquettes = false, $title='') {

	$total = $total_ok + $total_relire + $total_modif + $total_new;
	$percent = array();
	$percent_round = array();
	$percent['modif'] = round(100 * $total_modif / $total,1);
	$percent_round['modif'] = round($percent['modif']);
	$percent['relire'] = round(100 * $total_relire / $total,1);
	$percent_round['relire'] = round($percent['relire']);
	if ($total_ok > $total_new) {
		$percent['new'] = round(100 * $total_new / $total, 1);
		$percent_round['new'] = round($percent['new']);
		$percent['ok'] = 100 - array_sum($percent);
		$percent_round['ok'] = 100 - array_sum($percent_round);
	}
	else {
		$percent['ok'] = round(100 * $total_ok / $total,1);
		$percent_round['ok'] = round($percent['ok']);
		$percent['new'] = 100 - array_sum($percent);
		$percent_round['new'] = 100 - array_sum($percent_round);
	}

	if (!$title) {
		$title = _T('tradlang:info_module_traduit_pc', array('pc' => $percent['ok']));
	}

	$progress = "<div class=\"progress\" data-toggle=\"tooltip\" title=\"" . attribut_html($title) . "\">";
	foreach(['ok', 'relire', 'modif', 'new'] as $s) {
		$v = $percent[$s];
		$progress .= '<div class="progress-bar bg-' . statut_to_variant($s) . '" role="progressbar" style="width:'.$v.'%" aria-valuenow="'.$v.'" aria-valuemin="0" aria-valuemax="100">';
		if ($etiquettes==='all' or ($etiquettes and $s==='ok')) {
			$progress .= $percent_round[$s] . "%";
		}
		$progress .= '</div>';
	}
	$progress .= '</div>';
	return $progress;
}
