jQuery(function(){

	function setTooltips() {
		jQuery('[data-toggle="tooltip"],.bilan a,.bilan abbr,.bilan tr,.bilan td, textarea', this).tooltip();
		//$('.crayon-html textarea').tooltip();
	}

	setTooltips.apply(jQuery('body').get(0));
	onAjaxLoad(setTooltips);
});
