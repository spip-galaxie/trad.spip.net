<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/acces');
include_spip('inc/statistiques');
include_spip('prive/squelettes/inclure/stats-visites-jours_fonctions');

function stats_tradlang_total(?int $id_auteur = null, $serveur = ''): int {
	$where = [
		'objet = ' . sql_quote('tradlang'),
		'id_version > 0'
	];
	if ($id_auteur) {
		$where[] = 'id_auteur = ' . $id_auteur;
	}
	$total = sql_getfetsel('count(*) AS total_absolu', 'spip_versions', $where, '', '', '', '', $serveur);

	return $total ?: 0;
}
