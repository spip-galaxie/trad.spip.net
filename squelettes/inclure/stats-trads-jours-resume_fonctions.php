<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Calcule visites totales, aujourd'hui, hier pour le site ou id_auteur
 */
function statistiques_tradlang_stats_generales(array $Pile): array {

	$id_auteur = ($Pile[0]['id_auteur'] ?? null) ?: null;
	$table = 'spip_versions';
	$where = [
		'objet = ' . sql_quote('tradlang'),
		'id_version > 0'
	];
	if (!$id_auteur) {
		$where[] = 'id_auteur > 0';
	} else {
		$where[] = 'id_auteur = ' . $id_auteur;
	}

	$res = [];
	$aggregate = sql_get_select('COUNT(*) as max', $table, $where, 'DATE_FORMAT(date, "%Y-%m-%d")');
	$res['max'] = sql_getfetsel('MAX(max)', "($aggregate) AS source");
	$stats_visites_to_array = charger_fonction('stats_trads_to_array', 'inc');
	// on demande 2 jours de stats, à partir d'aujourd'hui
	$stats = $stats_visites_to_array('day', 2, $id_auteur);
	$data = array_column($stats['data'], 'revisions', 'date');
	// les lignes ne sortent que s'il y a des entrées...
	$res['today'] = $data[$stats['meta']['end_date']] ?? 0;
	$res['yesterday'] = $data[$stats['meta']['start_date']] ?? 0;
	return $res;
}
