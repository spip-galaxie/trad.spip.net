<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-galaxie/trad.spip.net.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// B
	'bouton_bon_a_pousser' => 'Bon à envoyer',
	'bouton_bon_a_pousser_attente' => 'En attente d’envoi…',

	// L
	'lien_menu_distributions' => 'Distributions',
	'lien_menu_tickets' => 'Les tickets'
);
