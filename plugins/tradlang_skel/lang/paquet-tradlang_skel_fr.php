<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-galaxie/trad.spip.net.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'tradlang_skel_description' => 'Le squelette utilisé par [le Tradlang officiel->https://trad.spip.net].',
	'tradlang_skel_slogan' => 'Squelette pour Tradlang'
);
