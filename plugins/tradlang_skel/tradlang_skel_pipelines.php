<?php
/**
 * Trad-lang v2 Squelette
 * Plugin SPIP de traduction de fichiers de langue
 * © kent1
 *
 * Fichier des pipelines utilisés par le plugin
 *
 * @package SPIP\Tradlang Skel\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Insertion dans le pipeline insert_head (SPIP)
 *
 * @param string $flux
 * 		Le contenu de la balise #INSERT_HEAD
 * @return string $flux
 * 		Le contenu de la balise modifié
 */
function tradlang_skel_insert_head($flux) {
	return $flux;
}

/**
 * Insertion dans le pipeline insert_head_css (SPIP)
 *
 * On ajoute les deux feuilles de style dans le head :
 * - la calculée spip.php?page=tradlang.css qui recupere les css des plugins revisions et statistiques
 *
 * @param string $flux
 * 		Le contenu de la balise #INSERT_HEAD_CSS
 * @return string $flux
 * 		Le contenu de la balise modifié
 */
function tradlang_skel_insert_head_css($flux) {
	static $done = false;
	if (!$done) {
		$done = true;
		$css = produire_fond_statique("tradlang.css", array('ltr' => $GLOBALS['spip_lang_left']));
		$flux .= '<link rel="stylesheet" href="'.timestamp($css).'" type="text/css" />';
	}
	return $flux;
}
