<?php
/**
 * Trad-lang v2 Squelette
 * Squelette spécifique pour le plugin tradlang
 * © kent1
 *
 * Fichier des options spécifiques du plugin
 *
 * @package SPIP\Tradlang Skel\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/lang_liste');

$GLOBALS['url_arbo_types']['grappe'] = 'distribution';

if (!defined('_DIR_PLUGIN_GRAPPES') && !function_exists('critere_id_grappe_dist')) {
	function critere_id_grappe_dist($idb, &$boucles, $crit) {

	}
}

// Définition des blocs Z utilisés par le squelette
$GLOBALS['z_blocs'] = array('content','aside','extra','head','head_js','header','footer', 'top');
